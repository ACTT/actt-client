import React, { Component } from 'react';
import { Grid, Button, Segment, Form, Header, Breadcrumb, Transition, Message } from 'semantic-ui-react';

import { Link } from 'react-router-dom'

class Project extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            name: this.props.match.params.name,
            scanStarted: false,
            scanError: false,
        };

        this.handleTypeChange = this.handleTypeChange.bind(this);
        this.addHeader = this.addHeader.bind(this);
        this.handleDropdownChange = this.handleDropdownChange.bind(this);
        this.fadeOut = this.fadeOut.bind(this);
    }

    componentDidMount() {
        // console.log(this.state.name)
        // fetch('http://localhost:3001/projects/' + this.state.name)
        // fetch(localStorage.getItem('hostname-backend') === null || localStorage.getItem('hostname-backend') === '' ? "http://localhost:3001" + '/projects/' + this.state.name : 'http://' + localStorage.getItem('hostname-backend') + '/projects/' + this.state.name) 
        fetch(window.checkLocalStorage("/projects/" + this.state.name))
            .then(response => response.json())
            .then(project => {
                if (project.requests === undefined) project.requests = [];
                if (project.loginRequests === undefined) project.loginRequests = [];
                this.setState({ project, loading: false })
            }).catch(err => {
                this.setState({ err });
            });
    }

    handleTypeChange(e, data) {
        let req = { ...this.state.req };
        req.requestType = data.value;
        this.setState({ req });
    }

    addHeader() {
        // console.log('fired')
        let req = { ...this.state.req };
        req.headers.push({ name: '', value: '' });

        // console.log(req)

        this.setState({ req });
    }

    deleteHeader(name) {
        let req = { ...this.state.req };
        let headerIndex = req.headers.findIndex(h => h.name === name);

        req.headers.splice(headerIndex, 1);

        this.setState({ req });
    }

    

    deleteRequest(req, type, index) {
        let url = '';

        if (type === 'login') url = window.checkLocalStorage("/projects/" + this.state.name + "/deleteLoginRequest/" + req.userType)
        else url = window.checkLocalStorage("/projects/" + this.state.name + "/deleteRequest/" + req.id);

        fetch(url,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET",
            })
            .then(res => {
                let project = { ...this.state.project }
                if (type === 'login') project.loginRequests.splice(index, 1);
                else project.requests.splice(index, 1);
                this.setState({ project });
                // console.log(type)
            })
            .catch(function (res) { console.log(res) })
    }

    handleInputChange(data, firstProp, secProp, index) {
        let req = { ...this.state.req };
        // console.log(req[firstProp])
        if (firstProp) {
            req[firstProp][index][secProp] = data.value;
        }

        this.setState({ req });
    }

    handleDropdownChange(data, property) {
        let req = { ...this.state.req };
        req[property] = data.value;
        // console.log(data.value)

        this.setState({ req });
    }

    saveProject() {
        fetch(window.checkLocalStorage("/projects/"),
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.state.project)
            })
            .then(function (res) {  })
            .catch(function (res) { console.log(res) })
    }

    startScan() {
        fetch(window.checkLocalStorage("/projects/" + this.state.name + "/startscan/"),
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET",
            })
            .then((res) => {
                if (res.ok) {
                  this.setState({scanStarted: true});
                }
                else {
                    this.setState({scanError: true});
                }
              })
            .then(function (res) {  })
            .catch(function (res) { console.log(res) })
    }

    fadeOut() {
        setTimeout(() => {
          this.setState({ scanStarted: false, scanError: false })
        }, 500);
      }

    render() {
        console.log('render: PROJECT')

        if(this.state.err) return <h4 style={{marginLeft: '10px'}}>Couldn't connect to back-end, check in the settings if correct back-end is set!</h4>

        if (this.state.loading === true) {
            // return (<Loader active />);
            return (<div></div>);
        } else {
            return (
                <div className="page-content">
                <div className="custom-breadcrumb">
                    <Breadcrumb size='small' style={{ marginTop: '10px', marginLeft: '10px'}}>
                    <Link to="/">
                        <Breadcrumb.Section>Home</Breadcrumb.Section></Link>
                        <Breadcrumb.Divider icon='right chevron' />
                        <Breadcrumb.Section active>{this.state.project.name}</Breadcrumb.Section>   
                    </Breadcrumb>   
                    </div>
                    <Header as='h2' attached='top' className="results-header">
                        <span style={{ color: "#5882FA" }}>{this.state.project.name}</span>
                    </Header>
                    <Segment attached>
                        <Form>

                            <Grid>
                                <Grid.Row>
                                    <Grid.Column width={8}>
                                        <Header as='h3' attached='top' className="results-header">
                                            Login requests
                                        </Header>
                                        <Segment attached>
                                            <h3>Url's</h3>

                                            {this.state.project.loginRequests.map((req, index) =>
                                                <div key={index} style={{ height: '40px' }}>
                                                    <span>{req.url}</span>

                                                    <Button icon="trash" negative floated="right"
                                                        onClick={(e) => this.deleteRequest(req, 'login', index)}></Button>
                                                    <Link to={"/requests/" + this.state.project.name + '/login/' + req.userType}>
                                                        <Button icon="edit" floated="right" /></Link>
                                                </div>

                                            )}

                                            <Link to={"/requests/" + this.state.project.name + '/login/'}>
                                                <Button icon="plus" positive /></Link>


                                        </Segment>
                                    </Grid.Column>

                                    <Grid.Column width={8}>
                                        <Header as='h3' attached='top' className="results-header">
                                            Validate requests
                                        </Header>
                                        <Segment attached>
                                            <h3>Url's</h3>

                                            {this.state.project.requests.map((req, index) =>
                                                <div key={index} style={{ height: '40px' }}>
                                                    <span>{req.url}</span>
                                                    <Button icon="trash" floated="right" negative onClick={(e) => this.deleteRequest(req, 'validate', index)}></Button>
                                                    <Link to={"/requests/" + this.state.project.name + '/validate/' + req.id}><Button floated="right" icon="edit" /></Link>

                                                </div>

                                            )}

                                            <Link to={"/requests/" + this.state.project.name + '/validate'}>
                                                <Button icon="plus" positive /></Link>

                                        </Segment>
                                    </Grid.Column>

                                </Grid.Row>


                                <Grid.Row>
                                    <Grid.Column width={2}>
                                        <Button icon="bullseye" labelPosition='left' onClick={(e) => this.startScan()} content="Start scan" />
                                    </Grid.Column>
                                    <Grid.Column width={3}>
                                        <Transition visible={this.state.scanStarted} animation='scale' duration={500} onShow={this.fadeOut}>
                                            <Message style={{textAlign:"center"}} positive>
                                                <Message.Header>Scan started!</Message.Header>
                                            </Message>
                                        </Transition>
                                        <Transition visible={this.state.scanError} animation='scale' duration={500} onShow={this.fadeOut}>
                                            <Message style={{textAlign:"center"}} negative>
                                                <Message.Header>Check your configuration!</Message.Header>
                                            </Message>
                                        </Transition>
                                    </Grid.Column>
                                    
                                    
                                </Grid.Row>
                                {/* <Grid.Row>
                                    <Transition visible={!this.state.scanStarted} animation='scale' duration={500} onShow={this.fadeOut}>
                                        <Message float="left" negative>
                                            <Message.Header>Scan started!</Message.Header>
                                        </Message>
                                    </Transition>
                                </Grid.Row>
                                <Grid.Row>
                                    <Button icon="bullseye" labelPosition='left' onClick={(e) => this.startScan()} content="Start scan" />
                                </Grid.Row> */}

                            </Grid>

                        </Form>

                    </Segment>

                </div>


            );
        }


    }
}

export default Project;
