import React, { Component } from 'react';
import { Grid, Input, Form, Segment } from 'semantic-ui-react';

class Settings extends Component {

    constructor(props) {
        super(props);

        this.state = {
            hostname: localStorage.getItem('hostname-backend')
        };
    }

    handleItemClick = (e, { name }) => {
        console.log(name)
        this.setState({ activeItem: name })
    }

    handleInputChange(data) {
        console.log(data.value )
        localStorage.setItem('hostname-backend', data.value)
        this.setState({hostname: data.value })
    }

    render() {
        // const { activeItem } = this.state;

        return (
            <div className="App page-content">
            <Form>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={8}>
                        <Segment attached>
                            <h3>Hostname backend:</h3>                            
                            <Input value={this.state.hostname} onChange={(e, data) => this.handleInputChange(data)} />
                        </Segment>
                        </Grid.Column>
                    </Grid.Row>                  
                </Grid>                
            </Form>
            </div>         
            
        );
    }
}

export default Settings;
