import React, { Component } from 'react';
import { Table, Icon, Loader, Pagination, Button, Header, Segment, Breadcrumb, Transition, Message } from 'semantic-ui-react';
import { Link } from 'react-router-dom'

class Results extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            name: this.props.match.params.name,
            failed: false,
            running: false,
        };
        // console.log(this.state.name)
        this.onPageChange = this.onPageChange.bind(this);
    }

    componentDidMount() {
        fetch(window.checkLocalStorage('/projects/' + this.state.name))
            .then((res) => {
                // console.log(res.text())
                return res.json();
            }).then(project => {
                if (project.requests === undefined) project.requests = [];
                if (project.loginRequests === undefined) project.loginRequests = [];
                if (project.results === undefined) project.results = [];
                if (project.lastscan === undefined) {
                    project.lastscan = [];
                    project.lastscan.delta = {};
                }

                let scan = project.lastscan;
                let delta = project.lastscan.delta;

                let failed = scan.date !== project.lastAttemptedScanDate && !project.scanIsRunning;
                let running = project.scanIsRunning;

                let userTypes = [];

                project.loginRequests.forEach((req) => {
                    userTypes.push(req.userType);
                });



                if (project.goal === undefined) {
                    project.goal = {};
                }

                this.setState({ project, userTypes, scan, loading: false, delta, failed, running })
            }).catch(err => {
                this.setState({ err });
            });;
    }

    onPageChange(e, data) {
        if (data.activePage < 1 || data.activePage > this.state.project.results.length) {
            return;
        }

        let scan = { ...this.state.scan };
        scan = this.state.project.results[this.state.project.results.length - data.activePage];
        let delta = scan.delta;

        // console.log(scan)
        this.setState({ scan, delta })
    }

    setGoal(url, status, userType, btnType) {
        let project = { ...this.state.project };

        if (btnType === 'OK') status = Object.assign({}, { [userType]: status[userType] });
        else status = Object.assign({}, { [userType]: !status[userType] });

        if (project.goal[url]) project.goal[url] = { ...project.goal[url], ...status };
        else project.goal[url] = status;

        // console.log('PARAMS')
        // console.log(url, status, userType, btnType)

        // console.log(project.goal[url])
        // console.log(project.goal)

        fetch(window.checkLocalStorage("/projects/createOrUpdateProject/"),
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(this.state.project)
            })
            .then((res) => {
                return res.json();
            }).then((json) => {
                // console.log(json)
                this.setState({ project: json })
                // let projects = [...this.state.projects];
                // projects.push(json);
                // this.setState({ open: false, projects })
            })
            .catch(function (res) { console.log(res) })
    }

    existsInGoal(url, status, userType) {
        let project = { ...this.state.project };
        // let userStatus = status[userType];
        let goal = project.goal;

        if (url in goal) {
            if (userType in goal[url]) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    removeFromGoal(url, status, userType) {
        let project = { ...this.state.project };
        let goal = project.goal;

        delete goal[url][userType];

        if (Object.keys(goal[url]).length === 0) {
            delete goal[url];
        }

        project.goal = goal;

        fetch(window.checkLocalStorage("/projects/createOrUpdateProject/"),
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(project)
            })
            .then((res) => {
                return res.json();
            }).then((json) => {
                // console.log(json)
                this.setState({ project: json })
                // let projects = [...this.state.projects];
                // projects.push(json);
                // this.setState({ open: false, projects })
            })
            .catch(function (res) { console.log(res) })
    }

    render() {
        console.log('render: RESULTS')

        if (this.state.err) return <h4 style={{ marginLeft: '10px' }}>Couldn't connect to back-end, check in the settings if correct back-end is set!</h4>

        if (this.state.loading === true) {
            return <Loader active />

        } else {
            let date = new Date(this.state.scan.date);
            let d;
            let time;
            if (isNaN(date.getTime())) {
                d = 'Date';
            }
            else {
                d = date.toLocaleDateString();
                time = date.getHours() + ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes() + ':' + (date.getSeconds() < 10 ? '0' : '') + date.getSeconds();
            }

            return (
                <div className="page-content">
                    <div className="custom-breadcrumb">
                        <Breadcrumb size='small' style={{ marginTop: '10px', marginLeft: '10px' }}>
                            <Link to="/">
                                <Breadcrumb.Section>Home</Breadcrumb.Section></Link>
                            <Breadcrumb.Divider icon='right chevron' />
                            <Breadcrumb.Section active>Results ({this.state.project.name})</Breadcrumb.Section>
                        </Breadcrumb>
                    </div>

                    <Header as='h2' attached='top' className="results-header">
                        Scan results (<span style={{ color: "#5882FA" }}>{this.state.project.name}</span>)
                        <div style={{ float: 'right', color: '#848484' }}>{d} {time}</div>


                    </Header>
                    <Segment attached>
                        <Transition visible={this.state.failed} animation='scale' duration={500}>
                            <Message onDismiss={this.handleDismiss} negative>
                                <Message.Header>Last scan failed!</Message.Header>
                                <p>Check your configuration and make sure your target application is online. The results below are from a previous scan.</p>
                            </Message>
                        </Transition>
                        <Transition visible={this.state.running} animation='scale' duration={500}>
                            <Message onDismiss={this.handleDismiss} info>
                                <Message.Header>Scan running!</Message.Header>
                                <p>Scan is still running. The results below are from a previous scan.</p>
                            </Message>
                        </Transition>
                        <h3>Delta's</h3>

                        <Table celled color="yellow">
                            <Table.Header>
                                <Table.Row warning>
                                    <Table.HeaderCell>Status</Table.HeaderCell>
                                    <Table.HeaderCell>URL</Table.HeaderCell>
                                    <Table.HeaderCell>UserType</Table.HeaderCell>
                                    <Table.HeaderCell>Allowed</Table.HeaderCell>
                                    <Table.HeaderCell>Current conclusion in goal</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>

                                {this.state.delta &&

                                    Object.keys(this.state.delta).map((status) => (
                                        Object.keys(this.state.delta[status]).map((url) => (
                                            Object.keys(this.state.delta[status][url]).map((result) => (
                                                <Table.Row warning>
                                                    <Table.Cell style={{ width: '8%' }}><Icon name='attention' /> {status}</Table.Cell>
                                                    <Table.Cell style={{ width: '51%' }}>{url}</Table.Cell>
                                                    <Table.Cell>{result}</Table.Cell>
                                                    <Table.Cell>{this.state.delta[status][url][result] === false ?
                                                        <React.Fragment><p>Not allowed</p></React.Fragment> :
                                                        <React.Fragment><p>Allowed</p></React.Fragment>}</Table.Cell>

                                                    <Table.Cell>
                                                        {this.state.project.goal[url] !== undefined ?

                                                            this.state.project.goal[url][result] !== undefined ?

                                                                <span>{this.state.project.goal[url][result] === true ? 'Allowed' : 'Not allowed'}</span>
                                                                : null

                                                            : null
                                                        }

                                                    </Table.Cell>
                                                </Table.Row>
                                            ))))))}
                            </Table.Body>
                        </Table>

                        <h3>Results</h3>

                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>URL</Table.HeaderCell>
                                    <Table.HeaderCell>UserType</Table.HeaderCell>
                                    <Table.HeaderCell>Allowed</Table.HeaderCell>
                                    <Table.HeaderCell>Action</Table.HeaderCell>
                                    <Table.HeaderCell>Current conclusion in goal</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>

                                {Object.keys(this.state.scan).map((key) => (
                                    key !== 'date' && key !== 'delta' &&
                                    this.state.userTypes.map((userType) => (
                                        <Table.Row>
                                            <Table.Cell>{key}</Table.Cell>
                                            <Table.Cell>{userType}</Table.Cell>
                                            <Table.Cell>{this.state.scan[key][userType] === false ?
                                                <React.Fragment><p>Not allowed</p></React.Fragment> :
                                                <React.Fragment><p>Allowed</p></React.Fragment>}</Table.Cell>
                                            <Table.Cell className="small-cell">
                                                {this.existsInGoal(key, this.state.scan[key], userType) === true ?
                                                    <Button icon="refresh" color="orange" onClick={(e) => this.removeFromGoal(key, this.state.scan[key], userType, 'OK')} />
                                                    :
                                                    <React.Fragment>
                                                        <Button icon="check" positive onClick={(e) => this.setGoal(key, this.state.scan[key], userType, 'OK')} />
                                                        <Button icon="cancel" negative onClick={(e) => this.setGoal(key, this.state.scan[key], userType, 'NOK')} />
                                                    </React.Fragment>
                                                }

                                            </Table.Cell>

                                            <Table.Cell>
                                                {this.state.project.goal[key] !== undefined ?
                                                    // <span>{this.state.project.goal[key][userType]}</span>
                                                    this.state.project.goal[key][userType] !== undefined ?

                                                        <span>{this.state.project.goal[key][userType] === true ? 'Allowed' : 'Not allowed'}</span>
                                                        : null

                                                    : null
                                                }


                                            </Table.Cell>
                                        </Table.Row>

                                    ))
                                ))}
                            </Table.Body>

                            <Table.Footer fullWidth>
                                <Table.Row textAlign="center">

                                    <Table.HeaderCell colSpan='5'>
                                        <Pagination onPageChange={this.onPageChange}
                                            boundaryRange={10}
                                            defaultActivePage={1}
                                            ellipsisItem={null}
                                            firstItem={null}
                                            lastItem={null}
                                            siblingRange={1}
                                            totalPages={this.state.project.results.length}
                                        />
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table>
                    </Segment>
                    {/* <h1>{this.state.project.name}</h1> */}



                </div>


            );
        }


    }
}

export default Results;
