import React, { Component } from 'react';
import { Button, Header, Grid, Loader, Modal, Input, Message, Transition, Segment } from 'semantic-ui-react';
import { Link } from 'react-router-dom'

window.checkLocalStorage = function(url) {
    if(localStorage.getItem('hostname-backend') === null || localStorage.getItem('hostname-backend') === '') {
        return "http://localhost:3001" + url;
    } else {
        return "http://" + localStorage.getItem('hostname-backend') + url;
    }
}

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: null,
      activeIndex: 0,
      loading: true,
      projects: [],
      open: false,
      openDelete: false,
      newProject: {
        name: ''
      },
      scanStarted: false,
      projectNameError: false
    };

    this.createProject = this.createProject.bind(this);
    this.fadeOut = this.fadeOut.bind(this);
  }

  componentDidMount() { 

    // fetch(localStorage.getItem('hostname-backend') === null || localStorage.getItem('hostname-backend') === '' ? "http://localhost:3001/projects/all/" : 'http://' + localStorage.getItem('hostname-backend') + '/projects/all/')
    fetch(window.checkLocalStorage("/projects/all/"))
      .then(response => response.json())
      .then(projects => {
        // console.log(projects)
        this.setState({ projects, loading: false })

      }).catch(err => {
        this.setState({ err });
      });
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index

    this.setState({ activeIndex: newIndex })
  }

  show = size => () => this.setState({ size, open: true })
  close = () => this.setState({ open: false })
  closeDelete = () => this.setState({ openDelete: false })
  showDelete = (size, project, index) => this.setState({ size, openDelete: true, selectedProject: project, selectedIndex: index })

  createProject() {
    if (this.state.newProject.name === '') {
      this.setState({ projectNameError: true })
      return;
    }
    // fetch(localStorage.getItem('hostname-backend') === null || localStorage.getItem('hostname-backend') === '' ? "http://localhost:3001/projects/createOrUpdateProject/" : 'http://' + localStorage.getItem('hostname-backend') + '/projects/createOrUpdateProject/',
    fetch(window.checkLocalStorage("/projects/createOrUpdateProject/"),
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(this.state.newProject)
      })
      .then((res) => {
        return res.json();
      }).then((json) => {
        let projects = [...this.state.projects];
        projects.push(json);
        this.setState({ open: false, projects })
      })
      .catch(function (res) { console.log(res) })
  }

  initProject(data) {
    if (data.value !== "" && this.state.projectNameError) {
      this.setState({ projectNameError: false })
    }
    
    let newProject = { ...this.state.project };
    newProject.name = data.value;
    newProject.requests = [];
    newProject.loginRequests = [];
    this.setState({ newProject })
  }

  deleteProject(project, index) {
    // console.log(index)
    // fetch(localStorage.getItem('hostname-backend') === null || localStorage.getItem('hostname-backend') === '' ? "http://localhost:3001" + "/projects/" + project.name + '/deleteProjectByName' : 'http://' + localStorage.getItem('hostname-backend') + "/projects/" + project.name + '/deleteProjectByName',
    fetch(window.checkLocalStorage("/projects/" + project.name + "/deleteProjectByName"),
      {
        method: "GET",
      })
      .then((res) => {
        let projects = [...this.state.projects];
        projects.splice(index, 1);

        this.setState({ projects, openDelete: false })
      })
      .catch(function (res) { console.log(res) })
  }

  startScan(project, index) {
    // fetch(localStorage.getItem('hostname-backend') === null || localStorage.getItem('hostname-backend') === '' ? "http://localhost:3001" + "/projects/" + project.name + "/startscan/" : 'http://' + localStorage.getItem('hostname-backend') + "/projects/" + project.name + "/startscan/",
    // fetch("http://localhost:3001/projects/" + project.name + "/startscan/",
    fetch(window.checkLocalStorage("/projects/" + project.name + "/startscan/"),
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "GET",
      })
      .then((res) => {
        let s = { ...this.state };
        if (res.ok) {          
          s['scanStarted' + index] = true;          
        }
        else {
          s['scanError' + index] = true;
        }
        this.setState(s);
      })
      .catch(function (res) { console.log(res) })
  }

  fadeOut(index) {
    setTimeout(() => {
      let s = { ...this.state };
      s['scanStarted' + index] = false;
      s['scanError' + index] = false;      
      this.setState(s);
    }, 1500);
  }

  render() {
    console.log('render HOME:')
    const { open, openDelete, size } = this.state;

    if(this.state.err) return <h4 style={{marginLeft: '10px'}}>Couldn't connect to back-end, check in the settings if correct back-end is set!</h4>

    if (this.state.loading === true) {
      return <Loader active />

    } else {
      return (
        <div className="App page-content">

          <Message
            icon='inbox'
            header='Welcome to ACTT v1'
            content="Make your apps secure by testing access controls!  "
          />

          <Grid>
            <Grid.Row>
              <Grid.Column width={12}>
                <Header as='h3' block>
                  Projects
                  </Header>

                  {this.state.projects.map((project, index) =>
                  <React.Fragment key={index}>
                    <Header as='h4' attached='top' className="results-header">
                      <div style={{color: "#333333"}}>{project.name}
                      </div>
                    </Header>
                    <Segment attached>
                      <ul>
                        <li>{project.loginRequests.length + ' Login Requests'} - {project.requests.length + ' Validate Requests'}</li>
                      </ul>
                    
                      <div style={{ position:'absolute', right: 10, top: 20, width: '37%' }}>
                      
                        <Transition visible={this.state['scanStarted' + index] ? true : false} animation='scale' duration={500} onShow={() => this.fadeOut(index)}>
                          <Message className="message-left" positive>
                            <Message.Header>Scan started!</Message.Header>
                          </Message>
                        </Transition>
                        <Transition visible={this.state['scanError' + index] ? true : false} animation='scale' duration={500} onShow={() => this.fadeOut(index)}>
                          <Message className="message-left" negative>
                            <Message.Header>Check your configuration!</Message.Header>
                          </Message>
                        </Transition>

                        <div className="buttons-right">

                        <Button icon="bullseye" labelPosition='left'
                          onClick={(e) => this.startScan(project, index)} content="Start scan" />

                        <Link key={1} to={"/projects/results/" + project.name}>
                          <Button icon="chart area" /></Link>

                        <Link key={2} to={"/projects/" + project.name}>
                          <Button icon="edit" /></Link>

                        <Button icon="trash" negative
                          onClick={(e) => this.showDelete('tiny', project, index)}></Button> 
                          </div>
                      </div>
                    </Segment>
                  </React.Fragment>

                )}

              </Grid.Column>
              <Grid.Column width={4}>
                <Header as='h3' block>
                  Actions
                  </Header>

                <Button icon="plus" labelPosition='left' positive onClick={this.show('tiny')} content="Add Project" />
              </Grid.Column>
            </Grid.Row>
          </Grid>

          <Modal size={size} open={open} onClose={this.close}>
            <Modal.Header>Create project</Modal.Header>
            <Modal.Content>
              <b>Name:</b> <Input error={this.state.projectNameError} value={this.state.newProject.name} onChange={(e, data) => this.initProject(data)} />
            </Modal.Content>
            <Modal.Actions>
              <Button negative onClick={this.close}>No</Button>
              <Button positive icon='checkmark' labelPosition='right' content='Yes' onClick={this.createProject} />
            </Modal.Actions>
          </Modal>

          <Modal size={size} open={openDelete} onClose={this.closeDelete}>
            <Modal.Header>Delete project</Modal.Header>
            <Modal.Content>
              Are you sure you want to delete <b>{this.state.selectedProject !== undefined && this.state.selectedProject.name}</b> ?
            </Modal.Content>
            <Modal.Actions>
              <Button secondary onClick={this.closeDelete}>Cancel</Button>
              <Button negative icon='trash' labelPosition='right' content='Yes' onClick={() => this.deleteProject(this.state.selectedProject, this.state.selectedIndex )} />
            </Modal.Actions>
          </Modal>

        </div>
      );
    }

  }
}

export default Home;
