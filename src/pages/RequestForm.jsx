import React, { Component } from 'react';
import { Input, Table, Grid, Dropdown, TextArea, Button, Icon, Segment, Form, Loader, Message, Transition, Header, Breadcrumb } from 'semantic-ui-react';
import CreatableSelect from 'react-select/lib/Creatable';
import { Redirect, Link } from 'react-router-dom'
import Tooltip from '../components/Tooltip';


class RequestForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: this.props.match.params.id,
      name: this.props.match.params.name,
      loading: this.props.match.params.id === undefined ? false : true,
      action: this.props.match.params.id === undefined ? 'Create' : 'Update',
      req: {
        name: '',
        headers: [],
        userType: this.props.match.params.id,
        statusCodes: [],
        requestType: this.props.match.params.type,
        requestMethod: "get",
        body: {}
      },
      failed: false,
      succes: false,
      userTypeIsSet: false,
      validationChecks: [],
      statusCodesObjs: [],
      redirect: false,
      jsonBodyError: false,
      // urlError: false
    };

    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleValidationChecksChange = this.handleValidationChecksChange.bind(this);
    this.handleStatusCodeCreate = this.handleStatusCodeCreate.bind(this);
    this.addHeader = this.addHeader.bind(this);
    this.handleDropdownChange = this.handleDropdownChange.bind(this);
    this.fadeOut = this.fadeOut.bind(this);
    this.handleDismiss = this.handleDismiss.bind(this);
    this.handleBodyChange = this.handleBodyChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  componentDidMount() {
    let requestType = this.props.match.params.type;
    // console.log(requestType)
    let getUrl = '';

    if (requestType === 'login') getUrl = window.checkLocalStorage('/projects/' + this.state.name + '/getLoginRequest/' + this.state.req.userType);
    else getUrl = window.checkLocalStorage('/projects/' + this.state.name + '/getRequestById/' + this.state.id);

    if (this.state.action === 'Update') {
      // console.log('UPDATE')
      fetch(getUrl)
        .then(response => response.json())
        .then(data => {
          let validationChecks = [...this.state.validationChecks];
          let statusCodesObjs = [];
          if (data.statusCodes === undefined) {
            data.statusCodes = [];
          }
          else if (data.statusCodes.length > 0) {
            for (var code in data.statusCodes) {
              statusCodesObjs.push({ value: String(data.statusCodes[code]), label: String(data.statusCodes[code]) })
            }
            validationChecks.push('Error Code');
          }
          if (data.bodyRegEx !== undefined && data.bodyRegEx !== '') {
            validationChecks.push('Body');
          }
          if (data.HeaderRegEx !== undefined && data.HeaderRegEx !== '') {
            validationChecks.push('Header');
          }

          let headers = data.headers;
          data.headers = [];
          // data.body = JSON.stringify(data.body);

          Object.keys(headers).forEach(function (key) {
            data.headers.push({ name: key, value: headers[key] });
          });

          // console.log(data.headers)

          this.setState({ req: data, validationChecks, statusCodesObjs, loading: false, userTypeIsSet: true });
        }).catch(err => {
          this.setState({ err });
        });;
    }
  }

  handleTypeChange(e, data) {
    let req = { ...this.state.req };
    if (req.requestType !== data.value) {
      delete req.authType;
      delete req.authKey;
      delete req.tokenKey;
      delete req.userType;
      // console.log(data.value)
      if (data.value === 'validate') {
        req.url = '';
      }
      req.requestType = data.value;
      this.setState({ req });
    }
  }

  handleValidationChecksChange(e, data) {
    let req = { ...this.state.req };
    let difference = this.state.validationChecks.filter(x => !data.value.includes(x));
    if (difference[0] === 'Header') {
      delete req.HeaderRegEx;
    }
    else if (difference[0] === 'Body') {
      delete req.bodyRegEx;
    }
    else if (difference[0] === 'Error Code') {
      this.setState({ statusCodesObjs: [] });
    }
    this.setState({ validationChecks: data.value, req })
  }

  handleStatusCodeCreate(data) {
    let statusCodesObjs = [...this.state.statusCodesObjs];
    let obj = {};
    obj.value = data;
    obj.label = data;
    statusCodesObjs.push(obj);
    this.setState({ statusCodesObjs })
  }

  handleStatusCodeChange(value) {
    let statusCodesObjs = [...this.state.statusCodesObjs];
    statusCodesObjs = value;
    this.setState({ statusCodesObjs });
  }

  addHeader() {
    // console.log('fired')
    let req = { ...this.state.req };
    req.headers.push({ name: '', value: '' });

    // console.log(req)

    this.setState({ req });
  }

  deleteHeader(name) {
    let req = { ...this.state.req };
    let headerIndex = req.headers.findIndex(h => h.name === name);

    req.headers.splice(headerIndex, 1);

    this.setState({ req });
  }

  handleInputChange(data, firstProp, secProp, index) {
    let req = { ...this.state.req };
    // console.log(data)

    if (secProp) {
      req[firstProp][index][secProp] = data.value;
    } else {
      req[firstProp] = data.value;
      // console.log(data)
    }

    this.setState({ req });
  }

  handleBlur(a) {
    if (!this.state.req.url) {
      return;
    }

    let url = new RegExp('^(https?:\\/\\/)' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + //port
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i');
    if (this.state.req.url.length < 2083 && url.test(this.state.req.url)) {
      this.setState({ urlError: false });
      // console.log("url is goed")
    }
    else {
      this.setState({ urlError: true });
      // console.log("url is false")
    }
  }

  handleDropdownChange(data, property) {
    let req = { ...this.state.req };
    req[property] = data.value;

    this.setState({ req });
  }

  handleSave(next) {
    let req = { ...this.state.req };

    let statusCodesObjs = [...this.state.statusCodesObjs]
    req.statusCodes = [];
    for (var obj in statusCodesObjs) {
      req.statusCodes.push(Number(statusCodesObjs[obj].value))
    }

    let error = "";
    if (!req.url || req.url === "" || this.state.urlError) { error += "Url, "; }
    if (req.requestMethod === "post") {
      if (!req.body || req.body === "") { error += "Body, "; }
    }
    if (req.requestType === "validate") {
      if (!req.tokenKey || req.tokenKey === "") { error += "Auth Token key, "; }
      if (!this.state.validationChecks || this.state.validationChecks.length === 0) { error += "Validation checks, "; }
      if (this.state.validationChecks.includes("Error Code") && (!req.statusCodes || req.statusCodes.length === 0)) { error += "Error Code, "; }
      if (this.state.validationChecks.includes("Header") && (!req.HeaderRegEx || req.HeaderRegEx === "")) { error += "Header, "; }
      if (this.state.validationChecks.includes("Body") && (!req.bodyRegEx || req.bodyRegEx === "")) { error += "Body, "; }
    }
    if (req.requestType === "login") {
      if (!req.userType || req.userType === "") { error += "User Type, "; }
      if (!req.authType || req.authType === "") { error += "Auth Type, "; }
      if (req.authType !== "plaintext" && (!req.authKey || req.authKey === "")) { error += "Auth Token key, "; }
    }

    if (error !== "") {
      error = error.slice(0, error.length - 2)
      this.setState({ failed: error });
      return;
    }

    let reqType = this.state.req.requestType === 'login' ? 'login' : '';

    let headers = {};

    this.state.req.headers.map((header, index) => {
      return headers[header.name] = header.value;
    });

    req.headers = headers;

    if (this.state.req.userType === undefined && reqType === 'login') {
      this.setState({ failed: true });
    } else {

      fetch(window.checkLocalStorage("/projects/" + this.state.name + "/createOrUpdate" + reqType + "Request"),
        {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          method: "POST",
          body: JSON.stringify(req)
        })
        .then(res => {
          if (!next) {
            this.setState({ redirect: true })
          }
          else {
            let req = { ...this.state.req };
            if (req.requestType === 'validate') {
              req.url = '';
            }
            else {
              req.userType = '';
            }
            // req.id = '';            
            delete req.id;
            this.setState({ req, userTypeIsSet: false, succes: true });
          }
        })
        .catch(res => {
          this.setState({ failed: true });
          // console.log(res)
        })
    }
  }

  fadeOut() {
    setTimeout(() => {
      this.setState({ succes: false })
    }, 1000);
  }

  handleDismiss() {
    this.setState({ failed: false })
  }

  handleBodyChange(e, data) {
    try {
      if (data.value === "") {
        data.value = "{}";
      }
      let req = { ...this.state.req };
      req.body = JSON.parse(data.value);
      this.setState({ req, jsonBodyError: false });
    } catch (er) {
      //couldn't parse string, don't update object
      this.setState({ jsonBodyError: er })
    }
  }

  render() {
    console.log('render: REQUESTFORM')
    // console.log(this.state)
    let httpMethods = [
      {
        text: 'GET',
        value: 'get',
      },
      {
        text: 'POST',
        value: 'post',
      },
    ];

    let types = [
      {
        text: 'Login',
        value: 'login',
      },
      {
        text: 'Validate',
        value: 'validate',
      },
    ];

    let resTypes = [
      {
        text: 'Plaintext',
        value: 'plaintext',
      },
      {
        text: 'JSON',
        value: 'json',
      },
      {
        text: 'Header',
        value: 'header',
      },
    ];

    // let userTypes = [
    //   {
    //     text: 'user',
    //     value: 'user',
    //   },
    //   {
    //     text: 'admin',
    //     value: 'admin',
    //   },
    //   {
    //     text: 'moderator',
    //     value: 'moderator',
    //   },
    // ];

    let opts = [
      {
        text: 'Error Code',
        value: 'Error Code',
      },
      {
        text: 'Body',
        value: 'Body',
      },
      {
        text: 'Header',
        value: 'Header',
      },
    ];

    if(this.state.err) return <h4 style={{marginLeft: '10px'}}>Couldn't connect to back-end, check in the settings if correct back-end is set!</h4>

    if (this.state.loading) {
      return (<Loader active />);
    } else if (this.state.redirect) {
      return <Redirect to={"/projects/" + this.state.name} />
    } else {
      return (
        <div className="page-content">
          <div className="custom-breadcrumb">
            <Breadcrumb size='small' style={{ marginTop: '10px', marginLeft: '10px' }}>
              <Link to="/">
                <Breadcrumb.Section>Home</Breadcrumb.Section></Link>
              <Breadcrumb.Divider icon='right chevron' />
              <Link to={"/projects/" + this.state.name}>
                <Breadcrumb.Section>{this.state.name}</Breadcrumb.Section></Link>
              <Breadcrumb.Divider icon='right chevron' />
              <Breadcrumb.Section active>{this.state.req.requestType === 'login' ? 'Login' : 'Validate'} Request ({this.state.req.url})</Breadcrumb.Section>
            </Breadcrumb>
          </div>
          <Header as='h2' attached='top' className="results-header">
            {this.state.action} {this.state.req.requestType === 'login' ? 'Login' : 'Validate'} Request (<span style={{ color: "#5882FA" }}>{this.state.name}</span>)
                     <div style={{ float: 'right', marginTop: '7px' }}>
              <h4 style={{ color: '#848484' }}>{this.state.req.url} </h4 >
            </div>
          </Header>
          <Segment attached>
            <Form>

              <Grid>
                <Grid.Row>
                  <Grid.Column width={8}>
                    <Header as='h3' attached='top' className="results-header">
                      Request info
                    </Header>
                    <Segment attached>
                      <h3>Url:</h3>
                      <Input value={this.state.req.url} onChange={(e, data) => this.handleInputChange(data, 'url')} error={this.state.urlError} onBlur={(data) => this.handleBlur(data)} />

                      {this.state.req.requestType === 'login' ?
                        <div className="validationChecks">
                          <h3 style={{ display: 'inline-block' }}>User Type:</h3>
                          <Tooltip iconName="exclamation" content="A userType can only occur once per project" />
                          <Input disabled={this.state.userTypeIsSet} value={this.state.req.userType} onChange={(e, data) => this.handleInputChange(data, 'userType')} />
                        </div> : null}

                      <h3>Type:</h3>
                      <Dropdown placeholder='Select Type' fluid selection options={types} onChange={this.handleTypeChange} value={this.state.req.requestType} />

                      <h3>Method:</h3>
                      <Dropdown placeholder='Select Method' fluid selection options={httpMethods} value={this.state.req.requestMethod} onChange={(e, data) => this.handleDropdownChange(data, 'requestMethod')} />

                      <h3>Headers:</h3>

                      <Table celled>
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell>Title</Table.HeaderCell>
                            <Table.HeaderCell>Value</Table.HeaderCell>
                            <Table.HeaderCell style={{ width: '50px' }}>Action</Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                        <Table.Body>

                          {this.state.req.headers.map((header, index) =>
                            <Table.Row key={index}>
                              <Table.Cell>
                                <Input value={header.name} onChange={(e, data) => this.handleInputChange(data, 'headers', 'name', index)} />
                              </Table.Cell>
                              <Table.Cell>
                                <Input value={header.value} onChange={(e, data) => this.handleInputChange(data, 'headers', 'value', index)} /></Table.Cell>
                              <Table.Cell><Button icon="trash" negative onClick={(e) => this.deleteHeader(header.name)}></Button></Table.Cell>
                            </Table.Row>
                          )}

                        </Table.Body>

                        <Table.Footer fullWidth>
                          <Table.Row>
                            <Table.HeaderCell />
                            <Table.HeaderCell colSpan='2'>
                              <Button positive floated='right' icon labelPosition='left' primary size='small' onClick={this.addHeader}>
                                <Icon name='plus' /> Add Header</Button>
                            </Table.HeaderCell>
                          </Table.Row>
                        </Table.Footer>
                      </Table>

                      {this.state.req.requestMethod === 'post' ?
                        <div>
                          <h3>Body:</h3>
                          <TextArea
                            rows="5"
                            onChange={(e, data) => this.handleBodyChange(e, data)}
                            ref="textarea"
                            defaultValue={JSON.stringify(this.state.req.body)} />
                          {/* <h4>Output</h4> */}
                          <Transition visible={this.state.jsonBodyError} animation='scale' duration={500}>
                            <Message float="left" negative>
                              <Message.Header>{this.state.jsonBodyError.message}</Message.Header>
                            </Message>
                          </Transition>
                          <pre>
                            {JSON.stringify(this.state.req.body, null, 2)}
                          </pre>
                          {/* <Segment>
                            {JSON.stringify(this.state.valueAA, null, 2)}
                          </Segment> */}
                        </div> : null
                      }

                    </Segment>
                  </Grid.Column>

                  <Grid.Column width={8}>
                    <Header as='h3' attached='top' className="results-header">
                      Response info
                    </Header>
                    <Segment attached>

                      {this.state.req.requestType === 'login' ?
                        <div>
                          <h3 style={{ display: 'inline-block' }}>Auth Type</h3>
                          <Tooltip iconName="question" content="The auth type is the way the application returns a token" />
                          <Dropdown value={this.state.req.authType} placeholder='Select auth type' fluid
                            selection options={resTypes} onChange={(e, data) => this.handleInputChange(data, 'authType')} /> </div> : null}

                      {(this.state.req.requestType === 'login' && (this.state.req.authType === 'json' || this.state.req.authType === 'header')) ?
                        <React.Fragment>
                          <h3 style={{ display: 'inline-block' }}>Auth Token key</h3>
                          <Tooltip iconName="question" content="The auth token key is the place where the application puts the token" />
                          <Input value={this.state.req.authKey} onChange={(e, data) => this.handleInputChange(data, 'authKey')} />
                        </React.Fragment> : null
                      }


                      {this.state.req.requestType === 'validate' ?
                        <React.Fragment>
                          <h3 style={{ display: 'inline-block' }}>Auth Token key</h3>
                          <Tooltip iconName="question" content="The auth token key is the place where the application puts the token" />
                          <Input value={this.state.req.tokenKey} onChange={(e, data) => this.handleInputChange(data, 'tokenKey')} />

                          <div className="validationChecks">
                            <h3 style={{ display: 'inline-block' }}>Validation checks:</h3>
                            <Tooltip iconName="question" content="With validation checks you can specify when requests fail, so that the results of a request can be distinguished" />
                            <Dropdown placeholder='Skills' fluid multiple selection options={opts} onChange={this.handleValidationChecksChange} value={this.state.validationChecks} />

                            {this.state.validationChecks.includes('Error Code') ?
                              <div className="validationChecks">
                                <h3 style={{ display: 'inline-block' }}>Error code:</h3>
                                <Tooltip iconName="question" content="Error codes are the codes a webserver returns when a user is not-authorized" />
                                <CreatableSelect isMulti backspaceRemovesValue openMenuOnClick={false} onCreateOption={this.handleStatusCodeCreate} onChange={this.handleStatusCodeChange.bind(this)} value={this.state.statusCodesObjs} />
                              </div> : null
                            }
                            {this.state.validationChecks.includes('Body') ?
                              <div className="validationChecks">
                                <h3 style={{ display: 'inline-block' }}>Body RegEx:</h3>
                                <Tooltip iconName="question" content="Please specify a RegEx pattern for searching specific content in the body of the response, if there is a match, a user is unauthorized" />
                                <Input onChange={(e, data) => this.handleInputChange(data, 'bodyRegEx')} value={this.state.req.bodyRegEx} />
                              </div> : null
                            }
                            {this.state.validationChecks.includes('Header') ?
                              <div className="validationChecks">
                                <h3 style={{ display: 'inline-block' }}>Header RegEx:</h3>
                                <Tooltip iconName="question" content="Please specify a RegEx pattern for searching specific content in the header, if there is a match, a user is unauthorized" />
                                <Input onChange={(e, data) => this.handleInputChange(data, 'HeaderRegEx')} value={this.state.req.HeaderRegEx} />
                              </div> : null
                            }
                          </div>
                        </React.Fragment> : null}

                    </Segment>
                  </Grid.Column>
                </Grid.Row>
                <div style={{ clear: 'both' }} />
                <Grid.Row>
                  <Grid.Column width={8}>
                    <Transition visible={this.state.failed} animation='scale' duration={500}>
                      <Message onDismiss={this.handleDismiss} negative>
                        <Message.Header>Update failed!</Message.Header>
                        <p>Please fill in {this.state.failed}</p>
                      </Message>
                    </Transition>

                    <Transition visible={this.state.succes} animation='scale' duration={500} onShow={this.fadeOut}>
                      <Message positive>
                        <Message.Header>Request saved!</Message.Header>
                      </Message>
                    </Transition>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row >
                  <Grid.Column width={2}>
                    {this.renderRedirect}
                    <Button positive onClick={(e) => this.handleSave(false)}>Save</Button>
                    <Button positive onClick={(e) => this.handleSave(true)}>Save & Next</Button>
                  </Grid.Column>
                </Grid.Row>

              </Grid>
              <br />

            </Form>

          </Segment>



        </div >);

    }

  }
}

export default RequestForm;
