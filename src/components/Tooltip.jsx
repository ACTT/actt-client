import React, { Component } from 'react';
import { Popup, Icon} from 'semantic-ui-react';

class Tooltip extends Component {

    constructor(props) {
        super(props);

        

        this.state = {
            iconName: this.props.iconName,
            content: this.props.content
        };
    }

    render() {
        // console.log(this.props)
        return (
            <Popup wide
                trigger={<Icon size="small" name={this.state.iconName} inverted circular color="blue" style={{ marginLeft: '10px' }} />}
                content={this.state.content}
                inverted
            />
        );
    }
}

export default Tooltip;
