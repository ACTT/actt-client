import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from './Header';
import Home from './pages/Home';
// import Settings from './pages/Settings';
import Project from './pages/Project';
import Results from './pages/ResultsForm';
import Settings from './pages/Settings';
import './App.css';
import RequestForm from './pages/RequestForm';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {

    };
  }

  componentDidMount() {
    // fetch('https://api.mydomain.com')
    //   .then(response => response.json())
    //   .then(data => this.setState({ data }));
  }

  render() {

    return (
      <Router>
        <div>
          <Header />

          <Route exact path="/" component={Home} />
          {/* <Route path="/home" component={Home} /> */}
          {/* <Route path="/settings" component={Settings} /> */}
            <Route exact path="/projects/:name" component={Project} />
          <Route exact path="/settings" component={Settings} />
          <Route path="/projects/results/:name" component={Results} />
          <Route path="/requests/:name/:type/:id?" component={RequestForm} />
        </div>
      </Router>
    );
  }
}

export default App;
