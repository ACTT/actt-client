import React, { Component } from 'react';
import { Menu, Sidebar, Button, Input } from 'semantic-ui-react';
import logo from './logo.svg';
import { withRouter } from 'react-router';

class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            hostname: localStorage.getItem('hostname-backend')
        };
    }

    handleItemClick = (e, { name }) => {
        // console.log(this.props)
        this.props.history.push('/' + name);
        this.setState({ activeItem: name })
    }

  handleShowClick = () => {
      const { visible } = this.state;
      this.setState({ visible: !visible })
  
  }

  handleInputChange(data) {
    console.log(data.value )
    localStorage.setItem('hostname-backend', data.value)
    this.setState({hostname: data.value })
}
  
  handleSidebarHide = () => { 
    this.setState({ visible: false })
    window.location.reload();
  }
      
    render() {
        console.log("render HEADER")
        const { activeItem, visible } = this.state;

        return (
            <React.Fragment>
            <Menu inverted>
                <Menu.Item>
                    <img src={logo} className="App-logo" alt="logo" />
                    <h4 className="App-name">ACTT</h4>
                </Menu.Item>

                <Menu.Item
                    name=''
                    active={activeItem === ''}
                    onClick={this.handleItemClick}
                >
                    Home
      </Menu.Item>

      <Menu.Item
                    name='settings'
                    active={activeItem === 'settings'}
                    onClick={this.handleShowClick}
                >
                Settings
                    
      </Menu.Item>
            </Menu>

        <Sidebar
        as={Menu}
        animation='overlay'
        direction='right'
        inverted
        vertical
        visible={visible}
        >
            <Menu.Item as='a' header>
            <h3>Hostname backend:</h3>                            
                <Input value={this.state.hostname} onChange={(e, data) => this.handleInputChange(data)} />
                <div style={{clear: 'both', marginTop: '10px'}} />  
                <Button style={{width: "100%"}} content="Close" negative onClick={this.handleSidebarHide}></Button>
            </Menu.Item>
    </Sidebar>
</React.Fragment>


        );
    }
}

export default withRouter(Header);
